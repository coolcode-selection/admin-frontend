## 技术栈
- angular5及以上版本


## 启动
**确保node版本至少在6.9.x以上和 npm 3.x.x 以上 **
1. 从gitlab上clone项目到本地
2. 执行`npm install`
3. 执行`npm start`
4. 打开浏览器输入 http://localhost:4200 ,你应该就能看到已经写好的页面了


**备注** 
- 如果可以使用npm,请尽量使用npm
- 提交前请执行ng build --prod测试，打包失败会被打回，如果显示错误不是代码问题，请使用npm下载


