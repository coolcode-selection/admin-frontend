export function dealTime(oldTime) {
  let star = new Date(oldTime);
  let time=star.getFullYear() + '-' + (star.getMonth() + 1) + '-' + star.getDate() + ' ' + star.getHours() + ':' + star.getMinutes() + ':' + star.getSeconds();
  return time;
}

export function dealData(oldTime) {
  let star = new Date(oldTime);
  let time=star.getFullYear() + '-' + (star.getMonth() + 1) + '-' + star.getDate();
  return time;
}