import { Component, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-starter',
  templateUrl: 'starter.template.html'
})
export class StarterViewComponent implements OnDestroy, OnInit  {

  loading = false;
  public nav: any;
  route: string;

  public constructor() {
  }

  public ngOnInit(): any {
    // this.nav.className += " white-bg";
    this.nav = document.querySelector('nav.navbar');
    this.route = localStorage.getItem('homeNav');
  }

  public ngOnDestroy(): any {
    // this.nav.classList.remove("white-bg");
  }
}
