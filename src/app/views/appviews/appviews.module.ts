import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import { StarterViewComponent } from './starterview.component';
import { LoadingModule } from '../../loading/loading.module';


@NgModule({
  declarations: [
    StarterViewComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule,
    LoadingModule,
  ],
  exports: [
    StarterViewComponent,
  ],
})

export class AppviewsModule {
}
