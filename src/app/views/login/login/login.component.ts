import { Component, OnInit } from '@angular/core';
import { BasicComponent } from '../../../components/basic/basic.component';
import { LoginService } from '../login.service';
import { Router } from '@angular/router';
import { LoginMobile } from '../../../class/login-mobile';
import { LoginPassword } from '../../../class/login-password';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService]
})
export class LoginComponent extends BasicComponent implements OnInit {

  public login = '登录';
  public loginPassword: LoginPassword = new LoginPassword();
  public loginMobile: LoginMobile = new LoginMobile();
  public disadbledButton = false;
  public smsCode = '获取验证码';
  public disabledSmsCode = false;

  constructor (public loginService: LoginService,
               public router: Router) {
    super();
  }

  ngOnInit () {
    if (this.loggedIn(localStorage.getItem('token_ttl'))) {
      this.router.navigateByUrl('starterview');
    }
  }

  loggedIn (token_ttl) {
    const timestamp = new Date().getTime();
    token_ttl = token_ttl + '000';
    if (timestamp < token_ttl) {
      return true;
    }
    return false;
  }

  public doMobilePasswordLogin () {
    this.login = '登录中...';
    this.disadbledButton = true;
    this.loginService.mobilePasswordLogin(this.loginPassword).subscribe(
      (data) => {
        this.login = '登录';
        this.disadbledButton = false;
        localStorage.setItem('token', data.token);
        localStorage.setItem('token_ttl', data.token_ttl);
        localStorage.setItem('userInfo', JSON.stringify(data.user_info));
        localStorage.setItem('user_id', data.id);
        if (this.loggedIn(data.token_ttl)) {
          this.router.navigateByUrl('starterview');
        }
      },
      (error) => {
        console.log(error);
        this.login = '登录';
        this.disadbledButton = false;
        const err = JSON.parse(error._body);
        this.showGrowl('error', '错误', err.message);
      }
    );
  }

  public doMobileCodeLogin () {
    this.login = '登录中...';
    this.disadbledButton = true;
    this.loginService.mobileCodeLogin(this.loginMobile).subscribe(
      (data) => {
        this.login = '登录';
        this.disadbledButton = false;
        localStorage.setItem('token', data.token);
        localStorage.setItem('token_ttl', data.token_ttl);
        localStorage.setItem('userInfo', JSON.stringify(data.user_info));
        localStorage.setItem('user_id', data.id);
        if (this.loggedIn(data.token_ttl)) {
          this.router.navigateByUrl('starterview');
        }
      },
      (error) => {
        this.login = '登录';
        this.disadbledButton = false;
        const err = JSON.parse(error._body);
        this.showGrowl('error', '错误', err.message);
      }
    );
  }

  getSmsCode (mobile) {
    console.log(mobile);
    let nums = 60;
    this.disabledSmsCode = true;
    const time = setInterval(
      () => {
        this.smsCode = `${nums}秒后重试`;
        nums--;
        if (nums === 0) {
          clearInterval(time);
          this.smsCode = `获取验证码`;
          this.disabledSmsCode = false;
        }
      }, 1000
    );
    this.loginService.getSmsCode(mobile).subscribe(
      (data) => {
        // const msg = data;
        this.showGrowl('success', '成功', '短信验证码发送成功，请注意查收');
        this.disadbledButton = false;
      },
      (error) => {
        const err = JSON.parse(error._body);
        if (err.messages) {
          this.showGrowlErrorMsg(err.messages);
        } else {
          this.showGrowlErrorMsg(err.message);
        }
      }
    );
  }

}
