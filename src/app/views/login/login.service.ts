import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {BasicService} from '../../components/basic/basic.service';
import {environment} from '../../../environments/environment';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';

@Injectable()
export class LoginService extends BasicService{

  constructor(public http: Http){
    super();
  }

  mobilePasswordLogin (loginInfo) {
    return this.http.post(`${environment.API_URL}/auth/login/mobile`, loginInfo, this.opts).map((res: Response) => res.json());
  }

  getSmsCode (mobile) {
    return this.http.post(`${environment.API_URL}/auth/sms-code`, {'mobile': mobile}, this.opts).map((res: Response) => res.json());
  }

  mobileCodeLogin (loginInfo) {
    return this.http.post(`${environment.API_URL}/auth/login/sms-code`, loginInfo, this.opts).map((res: Response) => res.json());
  }

}
