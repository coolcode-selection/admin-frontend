import { Component, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import 'jquery-slimscroll';
import { LayoutService } from '../layout.service';
import { PageListService } from '../../../services/page-list.service';

declare const jQuery: any;

@Component({
  selector: 'app-navigation',
  templateUrl: 'navigation.template.html',
  providers: [LayoutService]
})

export class NavigationComponent implements AfterViewInit {

  admin: any;
  navigation: any = [];
  cacheMenuString = '';
  isCacheMenu = false;

  constructor (private router: Router,
               private layoutService: LayoutService,
               private route: ActivatedRoute,
               private pageListService: PageListService) {
  }

  ngAfterViewInit () {
    jQuery('#side-menu').metisMenu();

    const body = jQuery('body');
    body.addClass('fixed-sidebar');
    if (body.hasClass('fixed-sidebar')) {
      jQuery('.sidebar-collapse').slimscroll({
        height: '100%',
        railOpacity: 0.9
      });
    }
  }

  gotoRouterTab (router) {
    const a = location.href;
    const url = a.toString().split('#');
    if (router === url[1].toString()) {
      const b = this.getCurrentComponent();
      this.send(b);
    } else {
      this.router.navigateByUrl(router);
    }
  }

  getCurrentComponent () {
    let nextRoute = this.route.children[0].component;
    if (nextRoute === undefined) {
      nextRoute = this.route.children[0].children[0].component;
    }
    return nextRoute;
  }

  activeRoute (routename: string): boolean {
    return this.router.url.indexOf(routename) > -1;
  }

  send (a): void {
    this.pageListService.sendMessage(a);  // 发送信息消息
  }

}
