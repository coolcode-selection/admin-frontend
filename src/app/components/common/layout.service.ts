import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {environment} from '../../../environments/environment';

@Injectable()
export class LayoutService {

  opts: RequestOptions;

  constructor(private http: Http) {
    const headers: Headers = new Headers();
    headers.append('content-type', 'application/json; charset=utf-8');
    this.opts = new RequestOptions();
    this.opts.headers = headers;
  }

  logout() {
    // return this.http.post(environment.API_URL + '/auth/sign-out', admin, this.opts).map((res: Response) => res.json());
  }

  getNav() {
    // return this.http.post(environment.API_URL + '/auth/getNav', admin, this.opts).map((res: Response) => res.json());
  }
}
