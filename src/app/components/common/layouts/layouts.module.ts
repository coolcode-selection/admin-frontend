import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { BasicLayoutComponent } from './basicLayout.component';
import { NavigationComponent } from './../navigation/navigation.component';
import { FooterComponent } from './../footer/footer.component';
import { StarterViewComponent } from '../../../views/appviews/starterview.component';
import { TabsetComponent } from '../../tabset/tabset.component';
import { TabItemComponent } from '../../tabset/tab-item.component';
import { GrowlModule } from 'primeng/primeng';

@NgModule({
  declarations: [
    FooterComponent,
    BasicLayoutComponent,
    NavigationComponent,
    TabsetComponent,
    TabItemComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule,
    GrowlModule,
  ],
  exports: [
    FooterComponent,
    BasicLayoutComponent,
    NavigationComponent,
  ],
  entryComponents: [StarterViewComponent],
})

export class LayoutsModule {
}
