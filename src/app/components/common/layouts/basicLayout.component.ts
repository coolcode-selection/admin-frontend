import { ActivatedRoute, NavigationEnd, Router, NavigationStart, NavigationError } from '@angular/router';
import { Component, OnInit, OnChanges, OnDestroy, AfterViewInit } from '@angular/core';
import { detectBody } from '../../../app.helpers';
import { StarterViewComponent } from '../../../views/appviews/starterview.component';
import { MenuService } from '../../../services/menu.service';
import { PageListService } from '../../../services/page-list.service';
import { Subscription } from 'rxjs/Subscription';
import { HostListener } from '@angular/core';
import { LayoutService } from '../layout.service';

declare var jQuery: any;

interface PageEntity {
  header: string;
  comp: any;
  name: string;
}

@Component({
  selector: 'app-basic',
  templateUrl: 'basicLayout.template.html',
  providers: [MenuService, PageListService, LayoutService],
})

export class BasicLayoutComponent implements OnInit, OnChanges, OnDestroy, AfterViewInit {

  private menuData: Array<any> = [];
  public selectedPage = 'page1';
  public subscription: Subscription;
  public tabMsg = [];
  public navRefresh = true;
  @HostListener('window:resize') onResize() {
    detectBody();
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private menuService: MenuService,
    public  pageListService: PageListService,
    public  layoutService: LayoutService,
  ) {
  }

  ngOnInit() {
    detectBody();
    this.router.navigateByUrl('starterview');
    this.watchRouterChange();
  }

  ngOnChanges() {
  }

  dealHeader(name) {
    const str = localStorage.getItem('session-header');
    return name.replace('$', str);
  }

  private watchRouterChange() {
    this.router.events.subscribe(evt => {
      if (evt instanceof NavigationStart) {
      }
      if (evt instanceof NavigationEnd) {
        let pageComponent;
        let page;
        try {
          let nextRoute = this.route.children[0].component;
          if (nextRoute === undefined) {
            nextRoute = this.route.children[0].children[0].component;
          }
          pageComponent = nextRoute;
          page =  this.getPage(nextRoute['name']);
        } catch (e) {
        }

        // 如果已经存在，就不新建
        const res = this.isHaveComponent(pageComponent);
        if (!res) {
          const idx = this.pageListService.pageList.length + 1;
          this.pageListService.pageList.push({
            header: this.dealHeader(page.header),
            comp: pageComponent,
            name: page.name,
            canChange: page.canChange,
            oldHeader: page.header,
          });
          localStorage.setItem('last-page-route-component', JSON.stringify(pageComponent));
          page.oldHeader = page.header;
          localStorage.setItem('last-page-route', JSON.stringify(page));
          setTimeout(() => {
            this.selectedPage = page.name;
          });
        }else {
          if (this.pageListService.pageList[res.index].canChange) {
            this.pageListService.pageList[res.index].header = this.dealHeader(this.pageListService.pageList[res.index].oldHeader);
          }
          this.selectedPage = res.res.name;
          localStorage.setItem('last-page-route', JSON.stringify(res.res));
        }

      }
    });
  }

  private isHaveComponent(pageComponent) {
    for (let i = 0; i < this.pageListService.pageList.length; i++) {
      if (pageComponent === this.pageListService.pageList[i].comp) {
        return {res: this.pageListService.pageList[i], index: i };
      }
    }
    return false;
  }

  private getPage(key) {
    return this.menuService.menuData[key];
  }

  delPageList (event) {
    this.pageListService.pageList.splice(event, 1);
  }

  closeAllTab() {
    this.pageListService.pageList.splice(1, this.pageListService.pageList.length - 1);
    this.tabMsg.push({severity: 'success', summary: '', detail: '其他選項卡都已關閉'});
    setTimeout(() => {
      this.tabMsg = [];
    }, 3000);
  }

  closeOtherTab(event) {
    if (event === 0) {
      this.closeAllTab();
    }else {
      this.pageListService.pageList.splice(event + 1, this.pageListService.pageList.length - event - 1);
      this.pageListService.pageList.splice(1, event - 1);
    }
  }

  ngAfterViewInit(): void {
    this.subscription = this.pageListService.getMessage().subscribe(msg => {
      let flag = false;
      for (let i = 0; i < this.pageListService.pageList.length; i++) {
        if (msg === this.pageListService.pageList[i].comp) {
          // 判斷是否可以改變
          if (this.pageListService.pageList[i].canChange) {
            this.pageListService.pageList[i].header = this.dealHeader(this.pageListService.pageList[i].oldHeader);
          }

          this.selectedPage = this.pageListService.pageList[i].name;

          localStorage.setItem('last-page-route', JSON.stringify(this.pageListService.pageList[i]));
          flag = true;
        }
      }
      if (!flag) {
        const page =  JSON.parse(localStorage.getItem('last-page-route'));
        this.pageListService.pageList.push({
          header: this.dealHeader(page.oldHeader),
          comp: msg,
          name: page.name,
          canChange: page.canChange,
          oldHeader: page.header
        });
        localStorage.setItem('last-page-route', JSON.stringify(page));
        setTimeout(() => {
          this.selectedPage = page.name;
        });
      }
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
