import { ChildrenOutletContexts, PRIMARY_OUTLET } from '@angular/router';
import {
  Component,
  ComponentFactoryResolver,
  ElementRef,
  Input,
  OnChanges,
  OnInit,
  ReflectiveInjector,
  Renderer2,
  SimpleChanges,
  ViewChild,
  ViewContainerRef,
  OnDestroy,
} from '@angular/core';

import { TabsetComponent } from './tabset.component';

@Component({
  selector: 'app-tab-item',
  templateUrl: 'tab-item.component.html'
})
export class TabItemComponent implements OnInit, OnChanges, OnDestroy {

  public innerName: string;
  private _active = false;
  public get active() { return this._active; }
  public set active(val) {
    this._active = val;
    if (val) {
      this.renderer.addClass(this.elementRef.nativeElement, 'active');
    } else {
      this.renderer.removeClass(this.elementRef.nativeElement, 'active');
    }
  }

  @Input()
  public name: string;

  @Input()
  public header: string;

  @Input()
  public icon: string;

  @Input()
  public comp: any;

  @Input()
  public delIndex: number;

  @ViewChild('dynamicComponentContainer', { read: ViewContainerRef })
  public dynamicComponentContainer: ViewContainerRef;

  constructor(
    private elementRef: ElementRef,
    private renderer: Renderer2,
    private tabset: TabsetComponent,
    private resolver: ComponentFactoryResolver,
    private parentContexts: ChildrenOutletContexts
  ) {
  }

  ngOnInit() {
    this.tabset.tabItems.push(this);
    this.elementRef.nativeElement.className = 'app-tab-item tab-pane';
  }

  ngOnDestroy() {
    const index = this.tabset.tabItems.indexOf(this);
    if (index !== -1) {
      this.tabset.tabItems.splice(index, 1);
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.name) {
      this.innerName = this.name;
    }
    if (changes.comp) {
      // 动态加载组件
      this.loadComponent(this.comp);
    }
  }

  private loadComponent(component: any) {
    const context = this.parentContexts.getContext(PRIMARY_OUTLET);
    const injector = ReflectiveInjector.fromResolvedProviders([], this.dynamicComponentContainer.injector);
    const resolver = context.resolver || this.resolver;
    const factory = resolver.resolveComponentFactory(component);
    const componentIns = factory.create(injector);
    this.dynamicComponentContainer.insert(componentIns.hostView);
  }
}
