import './tabset.component.styl';
import { AfterViewInit, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { TabItemComponent } from './tab-item.component';
import {Router} from '@angular/router';
import { smoothlyMenu } from '../../app.helpers';
import {LayoutService} from '../common/layout.service';
declare const jQuery: any;

@Component({
  selector: 'app-tabset',
  templateUrl: 'tabset.component.html',
  providers: [LayoutService]
})

export class TabsetComponent implements OnInit, OnChanges, AfterViewInit {

  private _currentTabItem: TabItemComponent;
  public tabItems: TabItemComponent[] = [];
  public tabMarginLeft = 80;
  public tabSet: any;
  public dropdownTab = false;
  public activeTab: any;
  logoutText = '退出';

  @Input()
  public selected: string;

  @Input()
  public tabsLeft = false;

  @Output()
  public selectedChange = new EventEmitter();

  @Output()
  public delPageList = new EventEmitter();

  @Output()
  public closeAllTab = new EventEmitter();

  @Output()
  public closeOtherTab = new EventEmitter();

  constructor(public router: Router,
              private layoutService: LayoutService) { }

  ngOnInit() {
    this.tabSet = document.getElementsByClassName('router-tabs')[0];
  }

  delTabs(i: number) {
    if (!isNaN(i)) {
      const index = this.tabItems.indexOf(this._currentTabItem);
      if (index === i) {
        this.setActiveItem(this.tabItems[i - 1]);
      }
      this.tabItems.splice(i, 1);
      this.delPageList.emit(i);
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.selected) {
      this._processSelectedChange(this.selected);
    }
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this._setTabItemsName();
      this._processSelectedChange(this.selected);
    });
  }

  public setActiveItem(tabItem: TabItemComponent) {
    if (this._currentTabItem === tabItem) {
      return;
    }
    if (this._currentTabItem) {
      this._currentTabItem.active = false;
    }
    this._currentTabItem = tabItem;
    this._currentTabItem.active = true;
    this.selectedChange.emit(this._currentTabItem.innerName);
    setTimeout(() => {
      this.getCurrent();
    }, 1);
  }

  private _processSelectedChange(name: string) {
    const findTabItem = this.tabItems.find(x => x.innerName === name) || this.tabItems[0];
    if (findTabItem) {
      this.setActiveItem(findTabItem);
    }
  }

  private _setTabItemsName() {
    this.tabItems.forEach((item: TabItemComponent, idx: number) => {
      if (!item.innerName) {
        item.innerName = `tabpane-${idx}`;
      }
    });
  }

  closeAll() {
    this.closeAllTab.emit();
    this.tabMarginLeft = 80;
    setTimeout(() => {
      this.setActiveItem(this.tabItems[0]);
    });
    this.dropdownTab = false;
  }

  seeRightTabs() {
    const lists = this.getAllWidth();
    let width = 0;
    const navWidth = this.getNavWidth();
    for (let index = 0; index < lists['lists'].length; index++) {
      width += Number.parseInt(lists['lists'][index]);
    }
    const hiddenRight = width - Math.abs(this.tabMarginLeft) - navWidth;
    if (hiddenRight < 80) {
      return false;
    }
    this.tabMarginLeft -= navWidth - 100;
    if (this.tabMarginLeft > 80) {
      this.tabMarginLeft = 80;
    }
  }

  seeLeftTabs() {
    const navWidth = this.getNavWidth();
    if (this.tabMarginLeft < 80) {
      this.tabMarginLeft += navWidth - 100; // 浮动值
    }
    if (this.tabMarginLeft > 80) {
      this.tabMarginLeft = 80;
    }
  }

  showDropDown() {
    if (this.dropdownTab) {
      this.dropdownTab = false;
    }else {
      this.dropdownTab = true;
    }
  }

  closeOther() {
    const index = this.tabItems.indexOf(this._currentTabItem);
    const a = this._currentTabItem;
    this.closeOtherTab.emit(index);
    // setTimeout(()=>{
    //   this.setActiveItem(this.tabItems[this.tabItems.length-1]);
    // }, 1);
    this.dropdownTab = false;
    this.tabMarginLeft = 80;
  }

  getCurrent() {
    const lists = this.getAllWidth();
    let width = 0;
    const navWidth = this.getNavWidth();
    let scollWidth = 0;
    for (let index = 0; index <= lists.active; index++) {
      width += Number.parseInt(lists['lists'][index]);
      if (width >= navWidth) {
        scollWidth += Number.parseInt(lists['lists'][index]);
      }
    }
    this.tabMarginLeft = 80 - scollWidth;
    if (this.tabMarginLeft > 80) {
      this.tabMarginLeft = 80;
    }
    this.dropdownTab = false;
  }

  logout(): void {
    this.logoutText = '退出中...';
    // this.layoutService.logout().subscribe(
    //   (data) => {
    //     this.cookieService.removeAll();
    //     localStorage.clear();
    //     this.router.navigateByUrl('login');
    //   },
    //   (error) => {
    //     this.cookieService.removeAll();
    //
    //   }
    // );
    localStorage.clear();
    this.router.navigateByUrl('login');
  }

  getAllWidth() {
    const items = document.querySelectorAll('.router-tabs li');
    const arr = [];
    let active;
    for (let index = 0; index < items.length; index++) {
      arr.push(items[index].clientWidth);
      if (items[index].classList.contains('active')) {
        active = index;
      }
    }
    return {lists: arr, active: active};
  }

  getCurrentLeft() {
    const a = document.getElementsByClassName('router-tabs')[0].getElementsByClassName('active')[0];
    return a['offsetLeft'];
  }

  getNavWidth() {
    return document.getElementsByClassName('nav-tab-set')[0].clientWidth - 220 - 292;
  }

  toggleNavigation(): void {
    jQuery('body').removeClass('fixed-sidebar');
    jQuery('body').toggleClass('mini-navbar');
    const body = jQuery('body');

    if (body.hasClass('fixed-sidebar')) {
      jQuery('.sidebar-collapse').slimscroll({
        height: '100%',
        railOpacity: 0.9
      });
    }
    smoothlyMenu();
  }
}
