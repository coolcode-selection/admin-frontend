import { OnInit } from '@angular/core';

import { BasicListComponent } from './basic-list.component';

export class BasicListWrapperComponent extends BasicListComponent implements OnInit {

  pageMeta: any;
  pageNumbers: Array<number>;
  currentPage = 1;
  oldPageSize = 10;

  sort = '';
  sortStatus = 'custom';
  keywords = '';
  tableEmptyMessage = '努力加载中...';
  loading = false;
  rightLoading = false;

  rows: any;
  listDetail: any;

  deleteTitle: string;
  deleteModal = false;
  saveMsg = false;
  msg = '';
  successMsg = 'success';

  constructor() {
    super();
  }

  ngOnInit() {
  }

  getList() {}

  changePage(event) {
    this.saveMsg = false;
    this.currentPage = event;
    this.sortStatus = 'false';
    this.getList();
  }

  onSort(event) {
    this.saveMsg = false;
    this.currentPage = 1;

    if (event.order > 0) {
      this.sort = event.field + '|asc';
    } else {
      this.sort = event.field + '|desc';
    }
    this.sortStatus = 'false';
    this.getList();
  }

  changePageSize(value) {
    this.saveMsg = false;
    this.currentPage = 1;
    this.oldPageSize = value;
    this.sortStatus = 'false';
    this.getList();
  }

  changeKeyword(value) {
    this.saveMsg = false;
    this.currentPage = 1;
    this.keywords = value;
    this.sortStatus = 'false';
    this.getList();
  }

  reload() {
    this.saveMsg = false;
    this.currentPage = 1;
    this.oldPageSize = 10;
    this.keywords = '';
    this.sort = '';
    this.sortStatus = 'false';
    this.getList();
  }

  showErrorMsg(msg) {
    this.saveMsg = true;
    this.successMsg = 'error';
    this.msg = msg;
  }

  showSuccessMsg(msg) {
    this.saveMsg = true;
    this.successMsg = 'success';
    this.msg = msg;
  }

  cancelDelete() {
    this.deleteModal = false;
  }

  deleteMsg() {
    this.saveMsg = false;
  }
}
