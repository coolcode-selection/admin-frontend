import { Subscription } from 'rxjs/Subscription';


export class BasicComponent {

  public msgs: any = [];
  public subscription: Subscription;
  public rightLoading: boolean;
  public previewImgModal = false;
  public previewImg = '';
  public manual: any;
  public testLink: any;
  saveMsg = false;
  msg = '';
  successMsg = 'success';

  constructor() { }

  showGrowl(severity = 'info', summary = '', detail = '') {
    this.msgs = [{severity: severity, summary: summary, detail: detail}];
  }

  showGrowlSuccessMsg(msg) {
    this.msgs = [{severity: 'success', summary: '', detail: msg}];
  }

  showGrowlErrorMsg(msg) {
    this.msgs = [{severity: 'error', summary: '错误', detail: msg}];
  }

  showErrorMsg (msg) {
    this.successMsg = 'error';
    this.msg = msg;
    this.saveMsg = true;
  }

  showSuccessMsg (msg) {
    this.successMsg = 'success';
    this.msg = msg;
    this.saveMsg = true;
  }

  closeMsg() {
    this.msgs = [];
  }

  deleteMsg () {
    this.saveMsg = false;
  }

  cancelImg() {
    this.previewImgModal = false;
  }

  seeImg(value) {
    this.previewImg = value;
    this.previewImgModal = true;
  }

  gotoTab(routerUrl, router, route, send: Function) {
    const a = location.href;
    const url = a.toString().split('#');
    if (routerUrl === url[1].toString()) {
      const b = this.getCurrentComponent(route);
      send(b);
    } else {
      router.navigateByUrl(routerUrl);
    }
  }

  protected getCurrentComponent(route) {
    let nextRoute = route.children[0].component;
    if (nextRoute === undefined) {
      nextRoute = route.children[0].children[0].component;
    }
    return nextRoute;
  }
}
