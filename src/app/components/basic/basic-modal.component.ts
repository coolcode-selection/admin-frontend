import { OnInit, OnDestroy } from '@angular/core';
import { BasicComponent } from './basic.component';

export class BasicModalComponent extends BasicComponent implements OnInit, OnDestroy {

  public body: any;

  constructor() {
    super();
    this.body = document.querySelector('body');
  }

  public ngOnInit(): any {
    this.body.classList.add('modal-body-scalls');
  }

  public ngOnDestroy(): any {
    this.body.classList.remove('modal-body-scalls');
  }

}
