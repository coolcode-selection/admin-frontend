import { Input, Output, EventEmitter } from '@angular/core';
import { BasicModalComponent } from './basic-modal.component';

export class BasicDetailComponent extends BasicModalComponent {

  @Output() cancelDetail = new EventEmitter();
  @Input() detailData;

  constructor() {
    super();
  }

  cancel() {
    this.cancelDetail.emit();
  }

}
