import {Input, OnInit} from '@angular/core';
import {BasicFormComponent} from './basic-form.component';

export class BasicFormWrapperComponent extends BasicFormComponent implements OnInit {

  constructor() {
    super();
  }

  ngOnInit() {
  }

  showGrowl(severity = 'info', summary = '', detail = '') {
    this.msgs = [{severity: severity, summary: summary, detail: detail}];
  }

  showSuccessMsg(msg) {
    this.msgs = [{severity: 'success', summary: '', detail: msg}];
  }

  showErrorMsg(msg) {
    this.msgs = [{severity: 'error', summary: '错误', detail: msg}];
  }

  closeMsg() {
    this.msgs = [];
  }

}
