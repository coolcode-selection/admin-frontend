import { Component, OnInit } from '@angular/core';
import { BasicComponent } from './basic.component';

export class BasicListComponent extends BasicComponent implements OnInit {

  pageMeta: Object;
  pageNumbers: Array<number>;
  rightLoading = false;
  loading = false;
  sort = '';
  oldPageSize = 10;
  keyWords = '';
  currentPage = 1;
  deleteTitle: string;
  deleteModal = false;
  listDetail: any;
  rows: any;
  sortStatus = 'custom';
  tableEmptyMessage = '努力加载中...';


  constructor() {
    super();
  }

  ngOnInit() {
  }

  changePage(event) {
    this.currentPage = event;
    this.sortStatus = 'false';
  }

  onSort(event) {
    this.currentPage = 1;

    if (event.order > 0) {
      this.sort = event.field + '|asc';
      return;
    }

    this.sort = event.field + '|desc';
    this.sortStatus = 'false';
  }

  changePagesize(value) {
    this.currentPage = 1;
    this.oldPageSize = value;
    this.sortStatus = 'false';
  }

  changeKeyword(value) {
    this.currentPage = 1;
    this.keyWords = value;
    this.sortStatus = 'false';
  }

  reload() {
    this.currentPage = 1;
    this.oldPageSize = 10;
    this.keyWords = '';
    this.sort = '';
    this.sortStatus = 'false';
  }

  cancelDelete() {
    this.deleteModal = false;
  }

  deleteList(value, deleteTitle) {
    this.deleteTitle = deleteTitle;
    this.deleteModal = true;
    this.listDetail = value;
  }
}
