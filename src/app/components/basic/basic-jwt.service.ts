import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {environment} from '../../../environments/environment';
import 'rxjs/add/operator/map';
import {BasicService} from './basic.service';

@Injectable()
export class BasicJwtService extends BasicService {


  constructor() {
    super();
    const token = localStorage.getItem('token');
    this.opts.headers.append('Authorization', `Bearer ${token}`);
  }

  setAuthor(author) {
    this.opts.headers.set('X-Author', author['Author']);
    this.opts.headers.set('X-Author-Password', author['password']);
  }
}
