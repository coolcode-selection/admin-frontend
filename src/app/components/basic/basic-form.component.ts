import { Input, OnInit } from '@angular/core';

import { BasicComponent } from './basic.component';
import { environment } from '../../../environments/environment';

export class BasicFormComponent extends BasicComponent implements OnInit {

  @Input() headTitle: string;
  @Input() isEdit: boolean;
  @Input() editId: number;
  @Input() testLink: any;
  languages: Object;
  model: any;
  loading = false;
  authorModal = false;
  oldData: object;
  group: Array<any>;
  manuals = false;


  constructor() {
    super();
  }

  ngOnInit() {
  }

  submit() {
    this.authorModal = true;
  }

  deleteMsg() {
    this.saveMsg = false;
  }

  cancelAuthor() {
    this.authorModal = false;
  }
}
