import {Component, OnInit, Input, OnDestroy, HostListener} from '@angular/core';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html'
})
export class LoadingComponent implements OnInit, OnDestroy {

  @Input() loading:boolean = false;
  height: number;
  scrollheight: number;
  linheight: number;
  rightLoading: any;
  public nav: any;

  @HostListener('window:scroll')
  onScroll() {
   this.onResize();
  }

  constructor() {
  }

  ngOnInit() {
    this.scrollheight = document.documentElement.scrollTop;
    this.height = document.body.offsetHeight;
  }

  public ngOnDestroy():any {

  }

  onResize() {
    this.height = document.body.offsetHeight;
    this.scrollheight = document.documentElement.scrollTop;
  }
}
