import { Injectable } from '@angular/core';

@Injectable()
export class MenuService {

  public menuData = {
    StarterViewComponent: {
      comp: 'StarterViewComponent',
      header: '首页',
      name: '首页',
      canChange: false  // 代表header是否可以改變
    },
    NewsListComponent: {
      comp: 'NewsListComponent',
      header: '資訊列表',
      name: '資訊列表',
      canChange: false
    },
    NewsCreateComponent: {
      comp: 'NewsCreateComponent',
      header: '新增資訊',
      name: '新增資訊',
      canChange: false
    },
    NewsEditComponent: {
      comp: 'NewsEditComponent',
      header: '編輯資訊',
      name: '編輯資訊',
      canChange: false
    },
    NewsLikeListComponent: {
      comp: 'NewsLikeListComponent',
      header: '點讚列表',
      name: '點讚列表',
      canChange: false
    },
    NewsCollectionListComponent: {
      comp: 'NewsCollectionListComponent',
      header: '收藏列表',
      name: '收藏列表',
      canChange: false
    },
    MessageListComponent: {
      comp: 'MessageListComponent',
      header: '所有訊息',
      name: '所有訊息',
      canChange: false
    },
    MessageUserComponent: {
      comp: 'MessageUserComponent',
      header: '戶口訊息',
      name: '戶口訊息',
      canChange: false
    },
    MessageUserDetailComponent: {
      comp: 'MessageUserDetailComponent',
      header: '訊息會話',
      name: '訊息會話',
      canChange: false
    },
    BannerPageComponent: {
      comp: 'BannerPageComponent',
      header: '廣告資訊管理',
      name: '廣告資訊管理',
      canChange: false
    },
    BannerCreateComponent: {
      comp: 'BannerCreateComponent',
      header: '新增廣告資訊',
      name: '新增廣告資訊',
      canChange: false
    },
    BannerEditComponent: {
      comp: 'BannerEditComponent',
      header: '編輯廣告資訊',
      name: '編輯廣告資訊',
      canChange: false
    },
    PositionComponent: {
      comp: 'PositionComponent',
      header: '位置管理',
      name: '位置管理',
      canChange: false
    },
    PositionCreateComponent: {
      comp: 'PositionCreateComponent',
      header: '新增廣告位置',
      name: '新增廣告位置',
      canChange: false
    },
    PositionEditComponent: {
      comp: 'PositionEditComponent',
      header: '編輯廣告位置',
      name: '編輯廣告位置',
      canChange: false
    },
    PositionAdsComponent: {
      comp: 'PositionAdsComponent',
      header: '$位置的廣告',
      name: '具体廣告位置',
      canChange: true
    },
    OrderComponent: {
      comp: 'OrderComponent',
      header: '所有訂單',
      name: '所有訂單',
      canChange: false,
    },
    AppsListComponent: {
      comp: 'AppsListComponent',
      header: '應用信息管理',
      name: '應用信息管理',
      canChange: false,
    },
    AppsCreateComponent: {
      comp: 'AppsCreateComponent',
      header: '新增應用',
      name: '新增應用',
      canChange: false,
    },
    AppsEditComponent: {
      comp: 'AppsEditComponent',
      header: '編輯應用',
      name: '編輯應用',
      canChange: false,
    },
    ProductComponent: {
      comp: 'ProductComponent',
      header: '產品設置',
      name: '產品設置',
      canChange: false,
    },
    ProductCreateComponent: {
      comp: 'ProductCreateComponent',
      header: '新增產品',
      name: '新增產品',
      canChange: false,
    },
    ProductEditComponent: {
      comp: 'ProductEditComponent',
      header: '編輯產品',
      name: '編輯產品',
      canChange: false,
    },
    ServiceTelComponent: {
      comp: 'ServiceTelComponent',
      header: '客服電話',
      name: '客服電話',
      canChange: false,
    },
    UserVersionComponent: {
      comp: 'UserVersionComponent',
      header: '戶口版本查詢',
      name: '戶口版本查詢',
      canChange: false,
    },
    AppsVersionUpdateComponent: {
      comp: 'AppsVersionUpdateComponent',
      header: '版本更新',
      name: '版本更新',
      canChange: false,
    },
    AppVersionTimelineComponent: {
      comp: 'AppVersionTimelineComponent',
      header: '版本詳情',
      name: '版本詳情',
    },
    OrderDetailComponent: {
      comp: 'OrderDetailComponent',
      header: '訂單詳情',
      name: '訂單詳情',
      canChange: false,
    },
    BasicComponent: {
      comp: 'BasicComponent',
      header: '基本设置',
      name: '基本设置',
      canChange: false
    },
    BasicEditComponent: {
      comp: 'BasicEditComponent',
      header: '编辑抽獎券活動',
      name: '编辑抽獎券活動',
      canChange: false
    },
    BasicCreateComponent: {
      comp: 'BasicCreateComponent',
      header: '新增抽獎券活動',
      name: '新增抽獎券活動',
      canChange: false,
    },
    PrizeComponent: {
      comp: 'PrizeComponent',
      header: '禮品管理',
      name: '禮品管理',
      canChange: false
    },
    PrizeCreateComponent: {
      comp: 'PrizeCreateComponent',
      header: '新增禮品',
      name: '新增禮品',
      canChange: false
    },
    PrizeEditComponent: {
      comp: 'PrizeEditComponent',
      header: '編輯禮品',
      name: '編輯禮品',
      canChange: false,
    },
    TypeComponent: {
      comp: 'TypeComponent',
      header: '抽獎券種類管理',
      name: '抽獎券種類管理',
      canChange: false
    },
    ExchangeComponent: {
      comp: 'ExchangeComponent',
      header: '禮品兌換',
      name: '禮品兌換',
      canChange: false
    },
    TypeCreateComponent: {
      comp: 'TypeCreateComponent',
      header: '新增抽獎券種類',
      name: '新增抽獎券種類',
      canChange: false,
    },
    TypeEditComponent: {
      comp: 'TypeEditComponent',
      header: '編輯抽獎券種類',
      name: '編輯抽獎券種類',
      canChange: false,
    },
    ActiveSendComponent: {
      comp: 'ActiveSendComponent',
      header: '$的抽獎券發放明細',
      name: '$的抽獎券發放明細',
      canChange: true
    },
    RensidenceComponent: {
      comp: 'RensidenceComponent',
      header: '戶口抽獎券明細',
      name: '戶口抽獎券明細',
      canChange: false,
    },
    UnuseComponent: {
      comp: 'UnuseComponent',
      header: '戶口未使用抽獎券列表',
      name: '戶口未使用抽獎券列表',
      canChange: false,
    },
    PreparationComponent: {
      comp: 'PreparationComponent',
      header: '戶口準備中抽獎券列表',
      name: '戶口準備中抽獎券列表',
      canChange: false
    },
    ReceiveComponent: {
      comp: 'ReceiveComponent',
      header: '戶口已領取抽獎券列表',
      name: '戶口已領取抽獎券列表',
      canChange: false,
    },
    UnreceiveComponent: {
      comp: 'UnreceiveComponent',
      header: '戶口未領取抽獎券列表',
      name: '戶口未領取抽獎券列表',
      canChange: false,
    },
    UnwinComponent: {
      comp: 'UnwinComponent',
      header: '戶口未中奖抽獎券列表',
      name: '戶口未中奖抽獎券列表',
      canChange: false
    },
    ProbabilityComponent: {
      comp: 'ProbabilityComponent',
      header: '更新禮品抽中概率',
      name: '更新禮品抽中概率',
      canChange: false,
    },
    PendComponent: {
      comp: 'PendComponent',
      header: '抽獎券發放管理',
      name: '抽獎券發放管理',
      canChange: false,
    },
    UnissuedComponent: {
      comp: 'UnissuedComponent',
      header: '未發放抽獎券',
      name: '未發放抽獎券',
      canChange: false
    },
    ReportComponent: {
      comp: 'ReportComponent',
      header: '抽獎券報表生成',
      name: '抽獎券報表生成',
      canChange: false,
    },
    CreateLuckyComponent: {
      comp: 'CreateLuckyComponent',
      header: '新增抽獎券',
      name: '新增抽獎券',
      canChange: false,
    },
    AdminSettingComponent: {
      comp: 'AdminSettingComponent',
      header: '設置後台權限',
      name: '設置後台權限',
      canChange: false
    },
    AdminPermissionComponent: {
      comp: 'AdminPermissionComponent',
      header: '後台權限參數管理',
      name: '後台權限參數管理',
      canChange: false
    },
    AdminPermissionCreateComponent: {
      comp: 'AdminPermissionCreateComponent',
      header: '新增後台權限參數',
      name: '新增後台權限參數',
      canChange: false
    },
    AdminPermissionEditComponent: {
      comp: 'AdminPermissionEditComponent',
      header: '編輯後台權限參數',
      name: '編輯後台權限參數',
      canChange: false
    },
    AdminRoleComponent: {
      comp: 'AdminRoleComponent',
      header: '後台角色權限管理',
      name: '後台角色權限管理',
      canChange: false
    },
    AdminRoleCreateComponent: {
      comp: 'AdminRoleCreateComponent',
      header: '新增角色',
      name: '新增角色',
      canChange: false
    },
    AdminRoleEditComponent: {
      comp: 'AdminRoleEditComponent',
      header: '編輯角色',
      name: '編輯角色',
      canChange: false
    },
    AdminRoleSettingComponent: {
      comp: 'AdminRoleSettingComponent',
      header: '设置角色後台權限',
      name: '设置角色後台權限',
      canChange: false
    },
    ModifyPasswordComponent: {
      comp: 'ModifyPasswordComponent',
      header: '修改密碼',
      name: '修改密碼',
      canChange: false
    },
    AdminBasicInfoComponent: {
      comp: 'AdminBasicInfoComponent',
      header: '後台用戶管理',
      name: '後台用戶管理',
      canChange: false
    },
    AdminBasicInfoEditComponent: {
      comp: 'AdminBasicInfoEditComponent',
      header: '編輯後台用戶',
      name: '編輯後台用戶',
      canChange: false
    },
    AdminBasicInfoCreateComponent: {
      comp: 'AdminBasicInfoCreateComponent',
      header: '新增後台用戶',
      name: '新增後台用戶',
      canChange: false
    },
    HelpComponent: {
      comp: 'HelpComponent',
      header: '幫助文檔',
      name: '幫助文檔',
      canChange: false
    },
    HelpCreateComponent: {
      comp: 'HelpCreateComponent',
      header: '新增文檔',
      name: '新增文檔',
      canChange: false
    },
    HelpEditComponent: {
      comp: 'HelpEditComponent',
      header: '編輯文檔',
      name: '編輯文檔',
      canChange: false
    },
    PlaneComponent: {
      comp: 'PlaneComponent',
      header: '艙位參數',
      name: '艙位參數',
      canChange: false
    },
    PlaneCreateComponent: {
      comp: 'PlaneCreateComponent',
      header: '新增艙位',
      name: '新增艙位',
      canChange: false
    },
    PlaneEditComponent: {
      comp: 'PlaneEditComponent',
      header: '编辑艙位',
      name: '编辑艙位',
      canChange: false
    },
    AreaComponent: {
      comp: 'AreaComponent',
      header: '地區參數',
      name: '地區參數',
      canChange: false
    },
    AreaCreateComponent: {
      comp: 'AreaCreateComponent',
      header: '新增地區',
      name: '新增地區',
      canChange: false
    },
    AreaEditComponent: {
      comp: 'AreaEditComponent',
      header: '编辑地區',
      name: '编辑地區',
      canChange: false
    },
    CompanyComponent: {
      comp: 'CompanyComponent',
      header: '公司參數',
      name: '公司參數',
      canChange: false
    },
    CompanyCreateComponent: {
      comp: 'CompanyCreateComponent',
      header: '新增公司',
      name: '新增公司',
      canChange: false
    },
    CompanyEditComponent: {
      comp: 'CompanyEditComponent',
      header: '编辑公司',
      name: '编辑公司',
      canChange: false
    },
    RechargeComponent: {
      comp: 'RechargeComponent',
      header: '充值規則參數',
      name: '充值規則參數',
      canChange: false
    },
    RechargeCreateComponent: {
      comp: 'RechargeCreateComponent',
      header: '新增充值規則',
      name: '新增充值規則',
      canChange: false
    },
    RechargeEditComponent: {
      comp: 'RechargeEditComponent',
      header: '编辑充值规则',
      name: '编辑充值规则',
      canChange: false
    },
    ManualsComponent: {
      comp: 'ManualsComponent',
      header: '後台用戶手冊',
      name: '後台用戶手冊',
      canChange: false
    },
    ManualsCreateComponent: {
      comp: 'ManualsCreateComponent',
      header: '新增後台用戶手冊',
      name: '新增後台用戶手冊',
      canChange: false
    },
    ManualsEditComponent: {
      comp: 'ManualsEditComponent',
      header: '編輯後台用戶手冊',
      name: '編輯後台用戶手冊',
      canChange: false
    },
    AccountTypeComponent: {
      comp: 'AccountTypeComponent',
      header: '戶口動態類型',
      name: '戶口動態類型',
      canChange: false
    },
    AccountTypeEditComponent: {
      comp: 'AccountTypeEditComponent',
      header: '编辑戶口動態類型',
      name: '编辑戶口動態類型',
      canChange: false
    },
    FeatureComponent: {
      comp: 'FeatureComponent',
      header: '程式應用功能參數',
      name: '程式應用功能參數',
      canChange: false
    },
    FeatureCreateComponent: {
      comp: 'FeatureCreateComponent',
      header: '新增程式應用功能',
      name: '新增程式應用功能',
      canChange: false
    },
    FeatureEditComponent: {
      comp: 'FeatureEditComponent',
      header: '编辑程式應用功能',
      name: '编辑程式應用功能',
      canChange: false
    },
    BasicParamPermissionsComponent: {
      comp: 'BasicParamPermissionsComponent',
      header: '查數 App 權限參數',
      name: '查數 App 權限參數',
      canChange: false
    },
    BasicParamPermissionsCreateComponent: {
      comp: 'BasicParamPermissionsCreateComponent',
      header: '新增查數 App 權限參數',
      name: '新增查數 App 權限參數',
      canChange: false
    },
    BasicParamPermissionsEditComponent: {
      comp: 'BasicParamPermissionsEditComponent',
      header: '編輯查數 App 權限參數',
      name: '編輯查數 App 權限參數',
      canChange: false
    },
    UserListComponent: {
      comp: 'UserListComponent',
      header: '戶口管理',
      name: '戶口管理',
      canChange: false
    },
    AccountListComponent: {
      comp: 'AccountListComponent',
      header: '戶口動態查詢',
      name: '戶口動態查詢',
      canChange: false
    },
    UserSyncMultipleComponent: {
      comp: 'UserSyncMultipleComponent',
      header: '同步新戶口',
      name: '同步新戶口',
      canChange: false
    },
    UserTelCreateComponent: {
      comp: 'UserTelCreateComponent',
      header: '新增戶口電話',
      name: '新增戶口電話',
      canChange: false
    },
    UserReportComponent: {
      comp: 'UserReportComponent',
      header: '戶口報表生成',
      name: '戶口報表生成',
      canChange: false
    },
    ServiceTelEditComponent: {
      comp: 'ServiceTelEditComponent',
      header: '編輯客服電話',
      name: '編輯客服電話',
      canChange: false
    },
    ServiceTelCreateComponent: {
      comp: 'ServiceTelCreateComponent',
      header: '新增客服電話',
      name: '新增客服電話',
      canChange: false
    },
    ManualsVedioComponent: {
      comp: 'ManualsVedioComponent',
      header: '用戶手冊詳情',
      name: '用戶手冊詳情',
      canChange: false
    },
    AdminsLogDetailComponent: {
      comp: 'AdminsLogDetailComponent',
      header: '用戶個人操作日誌',
      name: '用戶個人操作日誌',
      canChange: false
    },
    AdminsLogComponent: {
      comp: 'AdminsLogComponent',
      header: '後台用戶日誌',
      name: '後台用戶日誌',
      canChange: false
    },
    AdsLogComponent: {
      comp: 'AdsLogComponent',
      header: '廣告日誌',
      name: '廣告日誌',
      canChange: false
    },
    BasicParamLogComponent: {
      comp: 'BasicParamLogComponent',
      header: '基礎參數日誌',
      name: '基礎參數日誌',
      canChange: false
    },
    CompanyLogComponent: {
      comp: 'CompanyLogComponent',
      header: '公司日誌',
      name: '公司日誌',
      canChange: false
    },
    LuckydrawLogComponent: {
      comp: 'LuckydrawLogComponent',
      header: '抽獎券日誌',
      name: '抽獎券日誌',
      canChange: false
    },
    NewsLogComponent: {
      comp: 'NewsLogComponent',
      header: '資訊日誌',
      name: '資訊日誌',
      canChange: false
    },
    SystemPermissionLogComponent: {
      comp: 'SystemPermissionLogComponent',
      header: '後台權限日誌',
      name: '後台權限日誌',
      canChange: false
    },
    UsersLogComponent: {
      comp: 'UsersLogComponent',
      header: '戶口日誌',
      name: '戶口日誌',
      canChange: false
    },
    StatisticsComponent: {
      comp: 'StatisticsComponent',
      header: '數據統計',
      name: '數據統計',
      canChange: false,
    },
    EventDetailComponent: {
      comp: 'EventDetailComponent',
      header: '自定義事件詳情',
      name: '自定義事件詳情',
      canChange: false,
    },
  };
}
