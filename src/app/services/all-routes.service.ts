import {Injectable} from '@angular/core';

@Injectable()
export class AllRoutesService {

  public routes = [
    // 参数设置
    {path: '/param/areas'},
    {path: '/param/area/create'},
    {path: '/param/area/edit'},
    {path: '/param/institution-permission'},
    {path: '/param/institution-permission/create'},
    {path: '/param/institution-permission/edit'},
    {path: '/param/manuals'},
    {path: '/param/manual/create'},
    {path: '/param/manual/edit'},
    // 机构业务
    {path: '/institutions'},
    {path: '/institutions/create'},
    {path: '/institutions/edit'},
    // 学校业务
    {path: '/schools'},
    {path: '/schools/create'},
    {path: '/schools/edit'},
    // 员工中心
    {path: '/institution/staffs'},
    {path: '/institution/staffs/create'},
    {path: '/institution/staffs/edit'},
    {path: '/institution/staff-role'},
    {path: '/institution/staff-role/create'},
    {path: '/institution/staff-role/edit'},
    // 用户中心
    {path: '/user-center/user'},
    {path: '/user-center/user/create'},
    {path: '/user-center/user/edit'},
    {path: '/user-center/permission'},
    {path: '/user-center/permission/create'},
    {path: '/user-center/permission/edit'},
    // 教务中心
    {path: '/institution/teachers'},
    {path: '/institution/teachers/create'},
    {path: '/institution/teachers/edit'},
    {path: '/institution/classes'},
    {path: '/institution/classes/create'},
    {path: '/institution/classes/edit'},

  ];

}
