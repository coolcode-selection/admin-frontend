import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ManualRouteService {

  private subject = new Subject<any>();

  constructor() { }

  sendMessage(route) {
    this.subject.next(route);
  }

  /**
   * 清理消息
   */
  clearMessage() {
    this.subject.next();
  }
  /**
   * 获得消息
   * @returns {Observable<any>} 返回消息监听
   */
  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }

}


