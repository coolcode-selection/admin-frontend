import { Injectable } from '@angular/core';
import { StarterViewComponent } from '../views/appviews/starterview.component';
import { Subject } from 'rxjs/Subject';
import {Observable} from 'rxjs//Observable';

@Injectable()
export class PageListService {

  private subject = new Subject<any>();

  public pageList = [{
    header: '首页',
    comp: StarterViewComponent,
    name: '首页',
    canChange: false,
    oldHeader: '首页',
  }];

  constructor() { }

  sendMessage(pageComponent) {
    this.subject.next(pageComponent);
  }

  /**
   * 发送图片信息
   * @param src:图片地址
   */
  sendImages(src: string) {
    this.subject.next({url: src});
  }
  /**
   * 清理消息
   */
  clearMessage() {
    this.subject.next();
  }
  /**
   * 获得消息
   * @returns {Observable<any>} 返回消息监听
   */
  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }

}


