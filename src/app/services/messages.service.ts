import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class MessagesService {

  private subject = new BehaviorSubject<any>(0);

  sendMessage(obj) {
    this.subject.next(obj);
  }

  clearMessage() {
    this.subject.next(0);
  }

  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }

}


