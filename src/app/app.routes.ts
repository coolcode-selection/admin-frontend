import { Routes } from '@angular/router';
import { StarterViewComponent } from './views/appviews/starterview.component';
import { BasicLayoutComponent } from './components/common/layouts/basicLayout.component';
import { LoginComponent } from "./views/login/login/login.component";

export const ROUTES: Routes = [
  // Main redirect
  {path: '', redirectTo: 'starterview', pathMatch: 'full'},
  // {path: 'starterview', component: StarterViewComponent},
  {
    path: '', component: BasicLayoutComponent,
    children: [
      {path: 'starterview', component: StarterViewComponent},
    ]
  },
  {path: '**', redirectTo: 'starterview'}
];
